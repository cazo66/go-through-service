package br.com.ziben.rest;


import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.log4j.Logger;
import org.jboss.resteasy.annotations.GZIP;

import br.com.ziben.jms.JMSService;
import br.com.ziben.results.SuccessResult;

@Path("/touch")
public class TouchRest {

	private Logger log = Logger.getLogger("TouchRest");

	@POST
	@GZIP
	//@PermitAll
	@Path("/say")
	@Produces("application/json")
	public Response say(String json) {

		log.debug(">>TouchRest>say");
		System.out.println("Chegou isso: -->> "+ json);

		ResponseBuilder rb = null;

		JMSService jms = null;
		try {
			jms = new JMSService();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			jms.sendMessage("Envio de texto daqui do service");
		    rb = Response.ok(SuccessResult.success(200, "Texto enviado pra Queue com sucesso!"), MediaType.APPLICATION_JSON);
		} catch (Exception e) {
		    log.error("<<TouchRest<say deu pau geral! Olhem o estrago abaixo!", e);
		    rb = Response.status(500).entity("Um erro inesperado aconteceu, sinto muito!");
		} finally {
		    log.debug("<<TouchRest<say");
		}		
		return rb.build();
	}
}